package Data;

import java.util.ArrayList;
import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;
import Plans.IPlan;

public class PhoneNumberRepository implements IPhoneNumberRepository{

	List<Phone_Number> ListPhoneNumbers  = new ArrayList<Phone_Number>();
	List<Phone_Number> ListNewPhoneNumbers  = new ArrayList<Phone_Number>();

	public PhoneNumberRepository() {}
	
	public void RegisterNewPhoneNumbers(String listPhoneNumbers) {
		int number=0;
		double balance=0;
		String plan_type = null;
		String FriendNumbers = null;
		String[] parts = listPhoneNumbers.split("\\r?\\n|;");
		for(int i=0;i<parts.length;i++) {
			number=Integer.parseInt(parts[i]);i++;
			balance=Double.parseDouble(parts[i]);i++;
			plan_type=parts[i];i++;
			FriendNumbers=parts[i];
			Phone_Number newPhoneNumber = new Phone_Number(number, balance, plan_type, FriendNumbers);

			newPhoneNumber.showAllInformation();
			ListNewPhoneNumbers.add(newPhoneNumber);
		}
	}
	
	public List<Phone_Number> getNewPhoneNumbers(){
		return ListNewPhoneNumbers;
	}
	
	public void CleanNewPhoneNumbers() {
		ListNewPhoneNumbers.clear();
	}
	
	
	@Override
	public List<Phone_Number> getNumbers() {
		return ListPhoneNumbers;
	}
	
	@Override
	public void RegisterNewNumber(Phone_Number NewNumber) {
		ListPhoneNumbers.add(NewNumber);
	}

	@Override
	public void RegisterFriendNumber(int number, int friendNumber) {
		Phone_Number numberDB = getNumber(number);
		if (validateNumber(numberDB)) {
			numberDB.registerFriendNumber(friendNumber);
		}
	}

	@Override
	public void ShowPhoneNumber() {
		for (Phone_Number phone_Number : ListPhoneNumbers) {
			phone_Number.showAllInformation();
		}
	}
	
	@Override
	public Phone_Number getNumber(int number) {
		for (Phone_Number phone_Number : ListPhoneNumbers) {
			if (phone_Number.getTelephone()==number) {
				return phone_Number;
			}
		}
		return null;
	}
	
	private boolean validateNumber(Phone_Number numbeDB)
	{
		if (numbeDB==null) 
			return false;
		return true;
	}


	@Override
	public void LoadPhonesNumbers(List<Phone_Number> Numbers) {
		ListPhoneNumbers.clear();
		ListPhoneNumbers.addAll(Numbers);
	}
	

}
