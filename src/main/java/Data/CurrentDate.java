package Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class CurrentDate {
	
	public String getHour() {
		Calendar Calentar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));	
		DateFormat Hour = new SimpleDateFormat("HH:mm:ss");		
		String hourString = Hour.format(Calentar.getTime());
		return hourString;
	}
	public String getDate() {
		Calendar Calentar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		DateFormat Date = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = Date.format(Calentar.getTime());
		return dateString;
	}
}
