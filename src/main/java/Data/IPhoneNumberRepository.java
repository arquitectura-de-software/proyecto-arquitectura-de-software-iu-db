package Data;

import java.util.List;

import Entities.Phone_Number;

public interface IPhoneNumberRepository {
	List<Phone_Number>getNewPhoneNumbers();
	List<Phone_Number> getNumbers();
	void RegisterNewPhoneNumbers(String listPhoneNumbers);
	void RegisterNewNumber(Phone_Number NewNumber);
	void RegisterFriendNumber(int number,int friendNumber);
	void ShowPhoneNumber();
	void CleanNewPhoneNumbers();
	Phone_Number getNumber(int number);
	void LoadPhonesNumbers(List<Phone_Number> Numbers);
}
