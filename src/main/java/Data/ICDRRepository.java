package Data;

import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;

public interface ICDRRepository {
	CurrentDate currentDate = new CurrentDate();
	List<CDR> getCDRsCalculated();
	List<CDR> getCDRsNotCalculated();
	List<CDR> CalculateRateForOne(Phone_Number number);
	List<CDR> CalculateRateForAll(List<Phone_Number> number);
	void RegisterCDR(CDR CDR);
	void ShowAllCDRs();
	void LoadCDRs(List<CDR> CDRs);
	void RegisterCDRs(String CDRs);
	void CleanListCDRsNotCalculated();
	List<CDR> getCalculatedDateFromCDRs();
	List<CDR> getCDRFilterByDateAndHour(String CurrentDate, String CurrentHour);
}
