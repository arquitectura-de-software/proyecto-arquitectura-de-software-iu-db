package Controllers;
import static spark.Spark.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.Part;
import UseCases.ICalculateCostCDR;
import UseCases.ILoadAndSaveCDRandNumbersPersistence;
import UseCases.ILoadCDRsFromExternalFile;
import UseCases.IShowCDRs;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;


public class CDRsController {

	IShowCDRs showCDRs;
	ICalculateCostCDR calculateCostCDR;
	ILoadCDRsFromExternalFile loadCDRsFromExternalFile;
	ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence;
	public CDRsController(IShowCDRs showCDRs,
						  ICalculateCostCDR calculateCostCDR,
						  ILoadCDRsFromExternalFile loadCDRsFromExternalFile,
						  ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence) {
		this.showCDRs = showCDRs;
		this.calculateCostCDR = calculateCostCDR;
		this.loadCDRsFromExternalFile = loadCDRsFromExternalFile;
		this.loadAndSaveCDRandNumbersPersistence = loadAndSaveCDRandNumbersPersistence;
    	loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
	}
	

	
	public void Methods() {
	
		//http://localhost:8080/ //For Open de IU
    	port(8080);
        get("/", (request, response) ->
        {	
        	Map<String, Object> model = new HashMap<>();
        	model.put("Message","Pagina de Inicio");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/index.vm"));
        });
    	
        get("/getCostCDRs", (request, response) ->
        {
        	calculateCostCDR.CalculateCostForNewCDRs();
        	Map<String, Object> model = new HashMap<>();

        	model.put("CDRs", showCDRs.ShowCDRsNotCalculated());
        	model.put("TotalCDRs", showCDRs.ShowCDRsNotCalculated().size());
        	model.put("Title","�CDRs Tarificados!");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/registers/all.vm"));
        });
        
        get("/DeleteCDRsLoaded", (request, response) -> {
        	loadCDRsFromExternalFile.CleanCDRsLoaded();
        	Map<String, Object> model = new HashMap<>();
        	model.put("CDRs", showCDRs.ShowCDRsNotCalculated());
        	model.put("TotalCDRs", showCDRs.ShowCDRsNotCalculated().size());
        	model.put("Title","CDRs Eliminados");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/registers/all.vm"));
        });
       
        get("/recoverCDRsFromPersistence", (request, response) -> {
        	Map<String, Object> model = new HashMap<>();
        	loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
        	model.put("CDRs",showCDRs.ShowCDRsFromPersistence());
        	model.put("TotalCDRs", showCDRs.ShowCDRsFromPersistence().size());
        	model.put("Title","Datos Recuperados");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/CDRsTable.vm"));
        });

        get("/SaveCDRs", (request, response) -> {
        	Map<String, Object> model = new HashMap<>();
        	model.put("Message","�Los CDRs Se Guardaron Con Exito!");
        	calculateCostCDR.CalculateCostForNewCDRs();
        	loadAndSaveCDRandNumbersPersistence.SaveCDRPersistence(showCDRs.ShowCDRsNotCalculated());
        	loadCDRsFromExternalFile.CleanCDRsLoaded();
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/SaveMessage.vm"));
        });
        
        get("/ChangePersistence", (request, response) ->
        {
        	Map<String, Object> model = new HashMap<>();
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/ChangePersistence/ChangePersistencePage.vm"));
        });
        
        get("/loadFile", (request, response) -> {

        	Map<String, Object> model = new HashMap<>();
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/loadFile/IULoad.vm"));
        });
   
        get("/SQLPersistence", (request, response) ->
        {
        	Map<String, Object> model = new HashMap<>();
        	loadAndSaveCDRandNumbersPersistence.ChoosePersistence("SQL_PERSISTENCE");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/ChangePersistence/ChangePersistencePage.vm"));
        });

        get("/FilePersistence", (request, response) ->
        {
        	Map<String, Object> model = new HashMap<>();
        	loadAndSaveCDRandNumbersPersistence.ChoosePersistence("SERIALIZATED_PERSISTENCE");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/ChangePersistence/ChangePersistencePage.vm"));
        });
        
        post("/api/submit", (req,res)->{

        	req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/tmp"));
        	Part uploadedFile=null;
        	try {
        		uploadedFile=req.raw().getPart("myFile");
        	}
        	catch (IOException | ServletException e) {
        		e.printStackTrace();
        	}
        	try(InputStream inStream = uploadedFile.getInputStream()){
        		StringWriter writer = new StringWriter();
        		IOUtils.copy(inStream, writer);
        		String theString = writer.toString();
        		System.out.println("Contetn from uploaded file: " + theString);
        		loadCDRsFromExternalFile.LoadCDRsFromFileCSV(theString);
        		//NumbersAndCDRsInteractor.LoadTemporalData(theString);
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        	Map<String, Object> model = new HashMap<>();
        	
        	//model.put("CDRs", NumbersAndCDRsInteractor.getCDRsNotCalculatedfFromRepository());
        	model.put("CDRs", showCDRs.ShowCDRsNotCalculated());
        	model.put("TotalCDRs", showCDRs.ShowCDRsNotCalculated().size());
        	model.put("Title","CDRs Cargados");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/registers/all.vm"));
        });

        get("/SearchCDRRegister", (request, response) ->
        {
        	Map<String, Object> model = new HashMap<>();
        	//ShowFilterCDRs
        	loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
        	model.put("CDRs", showCDRs.ShowFilterCDRs());
        	model.put("Title", "Buscar por Fecha");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/SearchForDate.vm"));
        	
        });
        get("/api/getCDRsFilter", (request, response) ->
        {
        	Map<String, Object> model = new HashMap<>();
        	//ShowFilterCDRs
        	String Date = request.queryParams("myText");
        	String Hour = request.queryParams("myHour");
        	model.put("CDRs", showCDRs.ShowCDRsFilterByDateAndHour(Date,Hour));
        	model.put("TotalCDRs", showCDRs.ShowCDRsFilterByDateAndHour(Date,Hour).size());
        	model.put("Title","CDRs Encontrados");
        	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
    		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/CDRsTable.vm"));
        	
        });
	}
}
