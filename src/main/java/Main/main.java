package Main;

import Controllers.CDRsController;
import Controllers.PhoneNumbersController;
import Data.CDRRepository;
import Data.ICDRRepository;
import Data.IPhoneNumberRepository;
import Data.PhoneNumberRepository;
import Persistence.FileSQL;
import Persistence.IDataPersistence;
import UseCases.CalculateCostCDR;
import UseCases.ICalculateCostCDR;
import UseCases.ILoadAndSaveCDRandNumbersPersistence;
import UseCases.ILoadCDRsFromExternalFile;
import UseCases.ILoadPhoneNumberFromExternalFile;
import UseCases.IShowCDRs;
import UseCases.IShowPhoneNumbers;
import UseCases.LoadAndSaveCDRandNumbersPersistence;
import UseCases.LoadCDRsFromExternalFile;
import UseCases.LoadPhoneNumbersFromExternalFile;
import UseCases.ShowCDRs;
import UseCases.ShowPhoneNumbers;
public class main {
	
	public static void main(String[] args) {		
		//Starting Repositories
		IPhoneNumberRepository PhonesRepository = new PhoneNumberRepository();
		ICDRRepository CDRsRepository = new CDRRepository();
		IDataPersistence DataPersistence = new FileSQL();
		//Starting boundaries - Interactors (User Cases)
		ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence = new LoadAndSaveCDRandNumbersPersistence(DataPersistence,CDRsRepository,PhonesRepository);
		ILoadCDRsFromExternalFile loadCDRsFromExternalFile = new LoadCDRsFromExternalFile(CDRsRepository);
		ICalculateCostCDR calculateCostCDR = new CalculateCostCDR(CDRsRepository, PhonesRepository);
		IShowCDRs showCDRs = new ShowCDRs(CDRsRepository);
		IShowPhoneNumbers showPhoneNumbers = new ShowPhoneNumbers(PhonesRepository);
		ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile = new LoadPhoneNumbersFromExternalFile(PhonesRepository);
	
		//Creating the Controllers
		CDRsController controllerCDR= new CDRsController(showCDRs, calculateCostCDR, loadCDRsFromExternalFile, loadAndSaveCDRandNumbersPersistence);
		PhoneNumbersController phoneNumbers= new PhoneNumbersController(loadAndSaveCDRandNumbersPersistence,showPhoneNumbers,loadPhoneNumberFromExternalFile);
		controllerCDR.Methods();
		phoneNumbers.Methods();
	}
}
