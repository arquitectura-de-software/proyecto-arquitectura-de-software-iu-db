package Main;
import java.util.ArrayList;

import Entities.CDR;
import Entities.Phone_Number;
import Persistence.IDataPersistence;

public class Call_Center {
	
	private ArrayList<Phone_Number> listPhoneNumber = new ArrayList<Phone_Number>();
	private ArrayList<CDR> CDRs = new ArrayList<CDR>();

	public void registerNewPhone(Phone_Number phone) {
		listPhoneNumber.add(phone);
	}
	
	public void registerNewCDR(CDR register) {
		CDRs.add(register);
	}
	
	
	public Phone_Number searchPhone(int numberOrigin) {
		for(Phone_Number phone : listPhoneNumber)
		{
			if(phone.getTelephone() == numberOrigin)
				return phone;
		}
		return null;
	}
	
	public void getCostAllCDRs() {
		for(CDR register : CDRs)
		{
			Phone_Number phone = searchPhone(register.getOriginNumber());
			register.setCallCost(register.calculteCallCost(phone));
		}
	}
	
	public void serialization(IDataPersistence SerializationData) {
		SerializationData.SaveDataCDRs(CDRs);
	}
	
	public void ShowData_CDRs() {
		for(CDR register : CDRs)
		{
			register.showInformation();
		}
	}
}
