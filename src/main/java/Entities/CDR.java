package Entities;
import java.io.Serializable;

public class CDR implements Serializable{
	private static final long serialVersionUID = -2555170365942325277L;
	private int originNumber, destinationNumber;
	private double callCost; 
	private String durationCall, date, hour, dateTarification,hourTarification;
	
	public CDR(int originNumber,int destinationNumber, String durationCall, String date, String hour) {
		this.originNumber = originNumber;
		this.destinationNumber = destinationNumber;
		this.durationCall = durationCall;
		this.date = date;
		this.hour = hour;
		this.hourTarification="NO TARFICADO";
		this.dateTarification="NO TARFICADO";
	}
	
	public void SetdateTarification(String dateTarification) {
		this.dateTarification = dateTarification;
	}
	
	public void SethourTarification(String hourTarification) {
		this.hourTarification = hourTarification;
	}
	
	public String getdateTarification() {
		return dateTarification;
	}
	
	public String gethourTarification() {
		return hourTarification;
	}
	
	public int getOriginNumber() {
		return originNumber;
	}
	
	public int getDestinationNumber() {
		return destinationNumber;
	}
	
	public double getCallCost() {
		return callCost;
	}
	
	public String getDurationCall() {
		return durationCall;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getHour() {
		return hour;
	}
	

	public void setCallCost(double callCost) {
		this.callCost = callCost;
	}
	
	public double calculteCallCost(Phone_Number phone) {
		return phone.calculateCost(hour, durationCall, destinationNumber);
	}
	
	public void showInformation() {
		System.out.print("originNumber :: "+ originNumber);
		System.out.print(" destinationNumber :: "+ destinationNumber);
		System.out.print(" durationCall :: "+ durationCall);
		System.out.print(" date :: "+ date);
		System.out.println(" Cost :: "+ callCost);
	}
}
	
