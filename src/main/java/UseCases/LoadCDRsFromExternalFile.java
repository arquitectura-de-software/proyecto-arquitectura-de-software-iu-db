package UseCases;

import java.util.List;

import Data.ICDRRepository;
import Entities.CDR;

public class LoadCDRsFromExternalFile implements ILoadCDRsFromExternalFile {
	
	ICDRRepository CDRRepository;
	public LoadCDRsFromExternalFile(ICDRRepository CDRRepository) {
		this.CDRRepository = CDRRepository;		
	}

	public void LoadCDRsFromFileCSV(String ListCDR) {
		CDRRepository.RegisterCDRs(ListCDR);
	}

	@Override
	public void CleanCDRsLoaded() {
		CDRRepository.CleanListCDRsNotCalculated();
	}

}
