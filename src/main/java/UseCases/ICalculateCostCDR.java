package UseCases;

import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;

public interface ICalculateCostCDR {
	void CalculateCostForNewCDRs();
	void SetData(List<Phone_Number> List_Numbers);
}
