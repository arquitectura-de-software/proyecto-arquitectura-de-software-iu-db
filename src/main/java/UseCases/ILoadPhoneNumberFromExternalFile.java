package UseCases;

public interface ILoadPhoneNumberFromExternalFile {
	void LoadPhoneNumbersFromFileCSV(String PhoneNumbers);
	void CleanPhoneNumbersLoaded();
}
