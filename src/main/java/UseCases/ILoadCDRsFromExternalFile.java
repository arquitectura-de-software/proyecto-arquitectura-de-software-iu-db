package UseCases;

public interface ILoadCDRsFromExternalFile {
	void LoadCDRsFromFileCSV(String ListCDR);
	void CleanCDRsLoaded();
}
