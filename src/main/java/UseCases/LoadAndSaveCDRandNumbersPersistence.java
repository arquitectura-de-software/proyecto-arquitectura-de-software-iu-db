package UseCases;

import java.time.Period;
import java.util.List;

import Constructors.Persistence_Factory;
import Data.ICDRRepository;
import Data.IPhoneNumberRepository;
import Entities.CDR;
import Entities.Phone_Number;
import Persistence.IDataPersistence;

public class LoadAndSaveCDRandNumbersPersistence implements ILoadAndSaveCDRandNumbersPersistence{
	Persistence_Factory factory = new Persistence_Factory();
	IDataPersistence Persistence;
	ICDRRepository CDRRepository; 
	IPhoneNumberRepository PhoneNumberRepository;
	public LoadAndSaveCDRandNumbersPersistence(IDataPersistence persistence, ICDRRepository CDRRepository, IPhoneNumberRepository PhoneNumberRepository) {
		 this.Persistence = persistence;
		 this.CDRRepository = CDRRepository;
		 this.PhoneNumberRepository = PhoneNumberRepository;
	}
	@Override
	
	public void SaveCDRPersistence(List<CDR> ListCDRsCalculated) {
		Persistence.SaveDataCDRs(ListCDRsCalculated);
	}

	@Override
	public void LoadCDRPersistence() {
		CDRRepository.LoadCDRs(Persistence.LoadDataCDRs());
	}

	@Override
	public void LoadPhoneNumbersPersistence() {
		PhoneNumberRepository.LoadPhonesNumbers(Persistence.LoadDataPhoneNumbers());
	}
	@Override
	public void ChoosePersistence(String type) {
		this.Persistence = factory.ChangePersistence(type);		
	}
	@Override
	public void SavePhoneNumbersPersistence(List<Phone_Number> ListPhoneNumbers) {
		this.Persistence.SaveDataPhoneNumbers(ListPhoneNumbers);
	}
	@Override
	public String GetTypePersistence() {
		return Persistence.GetTypePersistence();
	}

}
