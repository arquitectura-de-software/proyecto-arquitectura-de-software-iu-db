package UseCases;

import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;

public interface ILoadAndSaveCDRandNumbersPersistence {
	void SavePhoneNumbersPersistence(List<Phone_Number> ListPhoneNumbers);
	void SaveCDRPersistence(List<CDR> ListCDRsCalculated);
	void LoadPhoneNumbersPersistence();
	void LoadCDRPersistence();
	void ChoosePersistence(String type);
	String GetTypePersistence();
}
