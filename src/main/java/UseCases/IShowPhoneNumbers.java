package UseCases;

import java.util.List;
import Entities.Phone_Number;

public interface IShowPhoneNumbers {
	List<Phone_Number> GetPhoneNumbersFromPersistence();
	List<Phone_Number> GetNewPhoneNumbers();
}
 