package UseCases;

import Data.IPhoneNumberRepository;

public class LoadPhoneNumbersFromExternalFile implements ILoadPhoneNumberFromExternalFile{
	IPhoneNumberRepository phoneNumberRepository;
	
	public LoadPhoneNumbersFromExternalFile(IPhoneNumberRepository phoneNumberRepository) {
		this.phoneNumberRepository = phoneNumberRepository;
	}
	
	@Override
	public void LoadPhoneNumbersFromFileCSV(String PhoneNumbers) {
		phoneNumberRepository.RegisterNewPhoneNumbers(PhoneNumbers);
	}

	@Override
	public void CleanPhoneNumbersLoaded() {
		phoneNumberRepository.CleanNewPhoneNumbers();
	}

}
