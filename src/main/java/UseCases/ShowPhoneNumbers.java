package UseCases;

import java.util.List;

import Data.IPhoneNumberRepository;
import Entities.Phone_Number;

public class ShowPhoneNumbers implements IShowPhoneNumbers{

	IPhoneNumberRepository PhoneNumberRepository;
	public ShowPhoneNumbers(IPhoneNumberRepository PhoneNumberRepository) {
		this.PhoneNumberRepository = PhoneNumberRepository;
	}
	@Override
	public List<Phone_Number> GetPhoneNumbersFromPersistence() {
		return PhoneNumberRepository.getNumbers();
	}
	@Override
	public List<Phone_Number> GetNewPhoneNumbers() {
		return PhoneNumberRepository.getNewPhoneNumbers();
	}
}
