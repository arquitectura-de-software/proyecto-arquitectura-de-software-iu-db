package UseCases;

import java.util.List;

import Entities.CDR;

public interface IShowCDRs {
	List<CDR> ShowCDRsNotCalculated();
	List<CDR> ShowCDRsFromPersistence();
	List<CDR> ShowFilterCDRs();
	List<CDR> ShowCDRsFilterByDateAndHour(String Date,String Hour);
}
