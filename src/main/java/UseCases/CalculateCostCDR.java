package UseCases;

import java.util.List;

import Data.ICDRRepository;
import Data.IPhoneNumberRepository;
import Entities.CDR;
import Entities.Phone_Number;

public class CalculateCostCDR implements ICalculateCostCDR{

	ICDRRepository cdrRepository;
	IPhoneNumberRepository PhoneNumberRepository;
	public CalculateCostCDR(ICDRRepository cdrRepository,IPhoneNumberRepository PhoneNumberRepository) {
		this.cdrRepository = cdrRepository;
		this.PhoneNumberRepository = PhoneNumberRepository;
	}
	@Override
	public void CalculateCostForNewCDRs() {
		cdrRepository.CalculateRateForAll(PhoneNumberRepository.getNumbers());
	}
	@Override
	public void SetData(List<Phone_Number> List_Numbers) {
		PhoneNumberRepository.LoadPhonesNumbers(List_Numbers);
	}

}
