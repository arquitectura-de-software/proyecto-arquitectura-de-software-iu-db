package Plans;

import java.io.Serializable;
import java.util.ArrayList;

public class Plan_Wow implements IPlan,Serializable{
	static final long serialVersionUID = -2555170365942325277L;
	private static final double NORMAL_PRICE = 0.99;
	ArrayList<Integer> listfriends= new ArrayList<Integer>(4);
	
	public Plan_Wow(ArrayList<Integer> listfriends) {
		this.listfriends=listfriends;
	}
	
	@Override
	public double calculateCost(String hour, String duration, int targetTelephone) {
		double cost = 0;
		if (!listfriends.contains(targetTelephone)) {
			cost = betweenTime.convertHourToSeconds(duration) * (NORMAL_PRICE/60);
		}
		cost = Math.round(cost * 100.0) / 100.0;
		return cost;
	}
	@Override
	public String getPlanType() {
		return "PLAN_WOW";
	}

}
