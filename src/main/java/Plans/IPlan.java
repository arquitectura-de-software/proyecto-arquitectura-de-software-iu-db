package Plans;

public interface IPlan {
	Time_Controller betweenTime = new Time_Controller();
	double calculateCost(String hour, String duration, int targetTelephone);
	String getPlanType();
}
