package Plans;

import java.io.Serializable;

public class Plan_Prepago implements IPlan,Serializable{
	static final long serialVersionUID = -2555170365942325277L;
	private static final double NORMAL_PRICE = 1.45;
	private static final double REDUCE_PRICE = 0.95;
	private static final double SUPER_REDUCE_PRICE = 0.70;

	//RANGO TARIFA NORMAL
	private static final String INITIAL_HOUR_NORMAL_PRICE = "06:59:59" ;
	private static final String FINAL_HOUR_NORMAL_PRICE = "21:00:00" ;

	//RANGO TARIFA REDUCIDA
	private static final String INITIAL_HOUR_REDUCE_PRICE = "20:59:59" ;
	private static final String FINAL_HOUR_REDUCE_PRICE = "01:00:00" ;

	//RANGO TARIFA SUPER_REDUCIDA
	private static final String INITIAL_HOUR_SUPER_REDUCE_PRICE = "00:59:59" ;
	private static final String FINAL_HOUR_SUPER_REDUCE_PRICE = "07:00:00" ;
	@Override
	public double calculateCost(String hour, String duration, int targetTelephone) {
		double cost = 0;
		if (betweenTime.isBetweenTime(INITIAL_HOUR_NORMAL_PRICE, FINAL_HOUR_NORMAL_PRICE, hour)) {
			cost = betweenTime.convertHourToSeconds(duration) * (NORMAL_PRICE/60);
		}
		if (betweenTime.isBetweenTime(INITIAL_HOUR_REDUCE_PRICE, FINAL_HOUR_REDUCE_PRICE, hour)) {
			cost = betweenTime.convertHourToSeconds(duration) * (REDUCE_PRICE/60);
		}
		if (betweenTime.isBetweenTime(INITIAL_HOUR_SUPER_REDUCE_PRICE, FINAL_HOUR_SUPER_REDUCE_PRICE, hour)) {
			cost = betweenTime.convertHourToSeconds(duration) * (SUPER_REDUCE_PRICE/60);
		}
		cost = Math.round(cost * 100.0) / 100.0;
		return cost;
	}
	@Override
	public String getPlanType() {
		return "PLAN_PREPAGO";
	}
}
