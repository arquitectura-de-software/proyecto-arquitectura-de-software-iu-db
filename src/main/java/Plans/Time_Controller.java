package Plans;

import java.text.SimpleDateFormat;
import java.time.LocalTime;

public class Time_Controller {
	
	private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
	
	private LocalTime initialTime;
	private LocalTime finalTime;
	private LocalTime intervalOfTime;
	
	boolean isBetweenTime(String InitialTime,String FinalTime, String IntervalOfTime) 
	{
		transformTime(InitialTime,FinalTime,IntervalOfTime);
		if (intervalOfTime.isBefore(finalTime))
			return initialTime.isAfter(finalTime) || initialTime.isBefore(intervalOfTime);
		else
			return initialTime.isAfter(finalTime) && initialTime.isBefore(intervalOfTime);
	}
	
	@SuppressWarnings("deprecation")
	private void transformTime(String InitialTime,String FinalTime, String IntervalOfTime) {
        try
        {
          java.util.Date InitialDate = sdf.parse(InitialTime); 
          java.util.Date FinalDate = sdf.parse(FinalTime); 
          java.util.Date IntervalDate = sdf.parse(IntervalOfTime); 
          
          initialTime = LocalTime.of(InitialDate.getHours(), InitialDate.getMinutes(),InitialDate.getSeconds());
          finalTime = LocalTime.of(FinalDate.getHours(), FinalDate.getMinutes(),FinalDate.getSeconds());
          intervalOfTime = LocalTime.of(IntervalDate.getHours(), IntervalDate.getMinutes(),IntervalDate.getSeconds());      
        } 
        catch (Exception e)
        {
          e.printStackTrace();
        }
	}

	@SuppressWarnings("deprecation")
	public double convertHourToSeconds(String Hour)
	{
		double totalSeconds;
	    try
	    {
	    	java.util.Date date = sdf.parse(Hour);        
	    	totalSeconds = ((date.getHours() * 3600) + (date.getMinutes() * 60) + (date.getSeconds()));
	    	return totalSeconds;
	    } 
	    catch (Exception e) 
	    {
          e.printStackTrace();
	    }
	    return 0;
	}
	
	
}
