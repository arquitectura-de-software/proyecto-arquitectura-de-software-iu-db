package Persistence;

import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;

public interface IDataPersistence {
	void SaveDataPhoneNumbers(List<Phone_Number> listPhoneNumbers);
	void SaveDataCDRs(List<CDR> ListCDR);
	List<CDR> LoadDataCDRs();
	List<Phone_Number> LoadDataPhoneNumbers();
	String GetTypePersistence();
}
