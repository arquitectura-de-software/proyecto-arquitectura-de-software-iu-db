package Persistence;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;
import java.io.FileInputStream;
public class FileTxt implements IDataPersistence {
	private static final String filepathCDRsSerializated="D:\\trabajo\\Septimo Semestre\\Arquitectura de software\\ProyectoArquitectura\\CDRSerializated.txt";
	private static final String filepathPhoneNumbersSerializated="D:\\trabajo\\Septimo Semestre\\Arquitectura de software\\ProyectoArquitectura\\PhoneNumbersSerializated.txt";
	public void SaveDataCDRs(List<CDR> CDRs) {
		List<CDR> listCDRsFromFile = LoadDataCDRs();
		try {
			FileOutputStream file = new FileOutputStream("CDRSerializated.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			if (listCDRsFromFile!=null)
				CDRs.addAll(listCDRsFromFile);		
			
			out.writeObject(CDRs);
			out.close();
			file.close();
		} catch (IOException ex) {
			System.out.println("Exception caught");
		}
	}


	@Override
	public List<CDR> LoadDataCDRs() {
	    FileTxt objectIO = new FileTxt();
        List<CDR> CDRsLoaded = (List<CDR>) objectIO.ReadObjectFromFile(filepathCDRsSerializated);
		return CDRsLoaded;
	}

	@Override
	public List<Phone_Number> LoadDataPhoneNumbers() {
	    FileTxt objectIO = new FileTxt();
        List<Phone_Number> ListPhoneNumber = (List<Phone_Number>) objectIO.ReadObjectFromFile(filepathPhoneNumbersSerializated);
		return ListPhoneNumber;
	}
	
    private Object ReadObjectFromFile(String filepath) {
        try {
            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            Object obj = objectIn.readObject();
            System.out.println("The Object has been read from the file");
            objectIn.close();
            return obj;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


	@Override
	public void SaveDataPhoneNumbers(List<Phone_Number> ListPhoneNumbers) {
		System.out.print(ListPhoneNumbers);
		try {
			FileOutputStream file = new FileOutputStream("PhoneNumbersSerializated.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(ListPhoneNumbers);
			out.close();
			file.close();
		} catch (IOException ex) {
			System.out.println("Exception caught");
		}
		
	}


	@Override
	public String GetTypePersistence() {
		return "SERIALIZATED_PERSISTENCE";
	}

}
