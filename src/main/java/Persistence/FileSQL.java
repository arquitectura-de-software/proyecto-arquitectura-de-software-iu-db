package Persistence;

import java.io.Console;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Data.CDRRepository;
import Data.ICDRRepository;
import Data.IPhoneNumberRepository;
import Entities.CDR;
import Entities.Phone_Number;

public class FileSQL implements IDataPersistence {

	@Override
	public void SaveDataCDRs(List<CDR> ListCDR) {
		int originNumber = 0, destinationNumber = 0;
		double callCost=0;
		String durationCall = null, date = null, hour = null,dateTarification =null,hourTarification=null;
		String url = "jdbc:mysql://localhost:3306/demo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Connection myConn = DriverManager.getConnection(url,user,password);
			Statement myStmt = myConn.createStatement();
			for (CDR cdr : ListCDR) {
				originNumber=cdr.getOriginNumber();
				destinationNumber=cdr.getDestinationNumber();
				durationCall=cdr.getDurationCall();
				date=cdr.getDate();
				hour=cdr.getHour();
				callCost= cdr.getCallCost();
				dateTarification = cdr.getdateTarification();
				hourTarification=cdr.gethourTarification();
				String sql = "insert into table_cdr"
						+ "(originNumber, destinationNumber, durationCall, date, hour,callCost,dateTarification,hourTarification)"
						+ " values ('"+originNumber+"', '"+destinationNumber+"', '"+durationCall+"', '"+date+"', '"+hour+"', '"+callCost+"','"+dateTarification+"','"+hourTarification+"')";
			
				myStmt.executeUpdate(sql);
			}			
			System.out.println("Insert complete");
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}		
	}


	@Override
	public List<CDR> LoadDataCDRs() {
		try {
			List<CDR> ListCDRs = new ArrayList<CDR>();
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
			Statement myStmt = myConn.createStatement();
			ResultSet myRss = myStmt.executeQuery("select * from table_cdr");
			while (myRss.next()) {
				CDR register = new CDR(myRss.getInt("originNumber"),myRss.getInt("destinationNumber"),myRss.getString("durationCall"),myRss.getString("date"),myRss.getString("hour"));
				register.SetdateTarification(myRss.getString("dateTarification"));
				register.SethourTarification(myRss.getString("hourTarification"));
				register.setCallCost(myRss.getDouble("callCost"));
				ListCDRs.add(register);
			}
	        return ListCDRs;

		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;

	}

	@Override
	public List<Phone_Number> LoadDataPhoneNumbers() {
		try {
			List<Phone_Number> ListNumbers = new ArrayList<Phone_Number>();
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
			Statement myStmt = myConn.createStatement();
	        ResultSet myRs = myStmt.executeQuery("select * from table_phone_numbers");
	        while (myRs.next()) {
	        	Phone_Number phone = new Phone_Number(myRs.getInt("telephone"),myRs.getDouble("balance"),myRs.getString("planType"),myRs.getString("friends_number"));
                ListNumbers.add(phone);
                phone.showAllInformation();
            
	        }
	        return ListNumbers;
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;
	}

	@Override
	public void SaveDataPhoneNumbers(List<Phone_Number> ListPhoneNumbers) {
		int number = 0;
		double balance = 0;
		String plan_type = null , FriendsNumbers=null;
		String url = "jdbc:mysql://localhost:3306/demo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Connection myConn = DriverManager.getConnection(url,user,password);
			Statement myStmt = myConn.createStatement();
			for (Phone_Number phoneNumber : ListPhoneNumbers) {
				number=phoneNumber.getTelephone();
				balance=phoneNumber.getBalance();
				plan_type=phoneNumber.getPlan();
				FriendsNumbers = phoneNumber.getTelephoneFriends();
				
				String sql = "insert into table_phone_numbers"
						+ "(telephone, balance, planType, friends_number)"
						+ " values ('"+number+"', '"+balance+"', '"+plan_type+"', '"+FriendsNumbers+"')";
			
				myStmt.executeUpdate(sql);
		
			}			
			System.out.println("Insert complete");
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}		

	}


	@Override
	public String GetTypePersistence() {
		return "SQL_PERSISTENCE";
	}

	
}