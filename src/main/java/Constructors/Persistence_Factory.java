package Constructors;

import Persistence.*;
import Persistence.IDataPersistence;

public class Persistence_Factory {

	public IDataPersistence ChangePersistence(String type) {
		IDataPersistence persistenceType = null;
		if ("SQL_PERSISTENCE".equals(type)) 
			persistenceType = new FileSQL();
        if ("SERIALIZATED_PERSISTENCE".equals(type)) 
        	persistenceType = new FileTxt();	
        return persistenceType;
	}
}
