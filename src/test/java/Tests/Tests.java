package Tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import Entities.CDR;
import Entities.Phone_Number;
import Main.main;
import Persistence.FileTxt;
import Persistence.IDataPersistence;
import Main.Call_Center;


class Tests {

	private Phone_Number Telefono_Plan_Postpago = new Phone_Number(1, 1, "PLAN_POSTPAGO",null);
	private Phone_Number Telefono_Plan_Prepago = new Phone_Number(1, 1, "PLAN_PREPAGO",null);
	private Phone_Number Telefono_Plan_WOW = new Phone_Number(1, 1, "PLAN_WOW",null);

	// VERIFY PLANS ARE CREATED CORRECTLY
	@Test
	void should_return_plan_type_postpago() {
		Telefono_Plan_Postpago.showAllInformation();
		assertEquals("PLAN_POSTPAGO", Telefono_Plan_Postpago.getPlan());
	}
	@Test
	void should_return_plan_type_prepago() {
		Telefono_Plan_Prepago.showAllInformation();
		assertEquals("PLAN_PREPAGO", Telefono_Plan_Prepago.getPlan());
	}
	@Test
	void should_return_plan_type_wow() {
		Telefono_Plan_WOW.showAllInformation();
		assertEquals("PLAN_WOW", Telefono_Plan_WOW.getPlan());
	}
	
	// VERIFY COST FOR PREPAGO PLAN

	@Test
	void should_return_cost_7_25() {
		assertEquals(7.25, Telefono_Plan_Prepago.calculateCost("07:00:00", "00:05:00", 1));
	}
	
	@Test
	void should_return_cost_7_98() {
		assertEquals(7.98, Telefono_Plan_Prepago.calculateCost("20:59:59", "00:05:30", 1));
	}
	
	@Test
	void should_return_cost_4_75() {
		assertEquals(4.75, Telefono_Plan_Prepago.calculateCost("21:00:00", "00:05:00", 1));
	}
	
	@Test
	void should_return_cost_5_23() {
		assertEquals(5.23, Telefono_Plan_Prepago.calculateCost("00:59:59", "00:05:30", 1));
	}

	@Test
	void should_return_cost_3_5() {
		assertEquals(3.5, Telefono_Plan_Prepago.calculateCost("01:00:00", "00:05:00", 1));
	}

	
	@Test
	void should_return_cost_3_85() {
		assertEquals(3.85, Telefono_Plan_Prepago.calculateCost("06:59:59", "00:05:30", 1));
	}

	// VERIFY COST FOR POSTPAGO PLAN
	
	@Test
	void should_return_cost_5_5() {
		assertEquals(5.5, Telefono_Plan_Postpago.calculateCost("06:59:59", "00:05:30", 1));
	}
	
	@Test
	void should_return_cost_12_0(){
		assertEquals(12.0, Telefono_Plan_Postpago.calculateCost("06:59:59", "00:12:00", 1));
	}
	
	
	// VERIFY COST FOR WOW PLAN

	@Test
	void should_return_cost_4_95() {
		assertEquals(4.95, Telefono_Plan_WOW.calculateCost("20:59:59", "00:05:00", 1));
	}
		
	//VERIFY FRIENDS NUMBERS
	@Test
	void should_return_0_when_targetnumber_is_123456() {
		Telefono_Plan_WOW.registerFriendNumber(123456);
		assertEquals(0, Telefono_Plan_WOW.calculateCost("20:59:59", "00:05:00", 123456));
	}
	
	@Test
	void should_return_0_when_targetnumber_is_not_123456() {
		Telefono_Plan_WOW.registerFriendNumber(123456);
		assertEquals(4.95, Telefono_Plan_WOW.calculateCost("20:59:59", "00:05:00", 654321));
	}
	
	// USING THE CENTRAL CALL CENTER
	
	@Test
	void testing_all_methods_void_should_not_return_null_or_exception() {
		
		Call_Center central = new Call_Center();
		Phone_Number phone_wow = new Phone_Number(321,20,"PLAN_WOW",null);
		CDR register = new CDR(321,1,"00:02:45", "DATE", "00:02:45");
		phone_wow.registerFriendNumber(1);
		central.registerNewPhone(phone_wow);
		central.registerNewCDR(register);
		central.getCostAllCDRs();
		central.ShowData_CDRs();
		IDataPersistence ExportData = new FileTxt();
		central.serialization(ExportData);
		assertEquals(0, register.getCallCost());
	}
	
	@Test
	void Test_search_phone_should_return_true_and_null_for_phone_not_finded() {

		Call_Center central = new Call_Center();
		Phone_Number phone_1 = new Phone_Number(321,20,"PLAN_POSTPAGO",null);
		Phone_Number phone_2 = new Phone_Number(123,20,"PLAN_WOW",null);
		Phone_Number phone_3 = new Phone_Number(442,20,"PLAN_PREPAGO",null);
		Phone_Number phone_4 = new Phone_Number(114,20,"PLAN_WOW",null);
		CDR register = new CDR(321,1,"00:02:45", "DATE", "00:02:45");
		CDR register2 = new CDR(4444,1,"00:02:45", "DATE", "00:02:45");
		central.registerNewPhone(phone_1);
		central.registerNewPhone(phone_2);
		central.registerNewPhone(phone_3);
		central.registerNewPhone(phone_4);
		assertEquals(phone_1, central.searchPhone(register.getOriginNumber()));
		assertEquals(null, central.searchPhone(register2.getOriginNumber()));
	}
	
	//FOR CDRs
	@Test
	void Test_methods_get_for_CDRs() {
		CDR register = new CDR(321,1,"00:02:45", "DATE", "00:02:45");

		assertEquals(321, register.getOriginNumber());
		assertEquals(1, register.getDestinationNumber());

	}
	
	//TESTING THE MAIN
	@Test
	void Testing_source_main() {
		main.main(null);
	}

}
